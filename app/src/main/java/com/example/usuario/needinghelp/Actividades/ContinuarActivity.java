package com.example.usuario.needinghelp.Actividades;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.usuario.needinghelp.Actividades.BaseDeDatosMg;
import com.example.usuario.needinghelp.R;
import com.example.usuario.needinghelp.TextosEsp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ContinuarActivity extends AppCompatActivity {

    private EditText contra, usuario, nombre, correo;
    private String contS, usuS, nombS, auxUs,corrS;
    private Button reg, inicio;
    private BaseDeDatosMg bd;
    private int cont, aux;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        reg = (Button) findViewById(R.id.regButt);
        inicio = (Button) findViewById(R.id.initsBttn);
        inicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), InicioSesionActivity.class);
                startActivity(intent);
            }
        });
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Registrarse();
            }
        });
        bd = new BaseDeDatosMg();
        contra = (EditText) findViewById(R.id.contraTxt);
        usuario = (EditText) findViewById(R.id.usuTxt);
        nombre = (EditText) findViewById(R.id.nombreTxt);
        correo = (EditText) findViewById(R.id.emailTxt);
    }

    public int verificarUsr(String us){
        String user = us;
        String contador="";
            try {
                Connection connection = bd.conexionBD();
                String peticion = TextosEsp.Reg()+user+"';";
                PreparedStatement consulta1 = connection.prepareStatement(peticion);
                ResultSet result1 = consulta1.executeQuery();
                result1.next();
                contador = result1.getString(1);
                connection.close();
            } catch (SQLException e) {
                Toast.makeText(getApplicationContext(), TextosEsp.ErrorReg(), Toast.LENGTH_SHORT).show();
            }
        return Integer.parseInt(contador);
    }

    public int verificarCorreo(String cr){
        String corr = cr;
        String contador="";
        try {
            Connection connection = bd.conexionBD();
            String peticion = TextosEsp.Corr()+cr+"';";
            PreparedStatement consulta1 = connection.prepareStatement(peticion);
            ResultSet result1 = consulta1.executeQuery();
            result1.next();
            contador = result1.getString(1);
            Toast.makeText(getApplicationContext(), contador, Toast.LENGTH_SHORT).show();
            connection.close();
        } catch (SQLException e) {
            Toast.makeText(getApplicationContext(), TextosEsp.ErrorReg(), Toast.LENGTH_SHORT).show();
        }
        return Integer.parseInt(contador);
    }

    public void Registrarse(){
        try {
            contS = contra.getText().toString();
            usuS = usuario.getText().toString();
            nombS = nombre.getText().toString();
            corrS = correo.getText().toString();
            Connection connection = bd.conexionBD();
            if(!usuS.equals("")) {
                auxUs = usuS;
                if(!nombS.equals("")) {
                    if (!corrS.equals("")) {
                        int contadorcorreo=0;
                        int auxcorr=0;
                        for (int i = 0; i<corrS.length();i++){
                            char a = corrS.charAt(i);
                            auxcorr++;
                            if(a=='@'){
                                contadorcorreo++;
                                break;
                            }
                        }
                        for(int i=auxcorr; i<corrS.length();i++){
                            char a = corrS.charAt(i);
                            if(a=='.'){
                                contadorcorreo++;
                                break;
                            }
                        }
                        if(contadorcorreo>1) {
                            if(!contS.equals("")) {
                                aux = verificarUsr(auxUs);
                                if(aux==0) {
                                    int auxcorreov = verificarCorreo(corrS);
                                    if(auxcorreov==0) {
                                        try {
                                            PreparedStatement pet2 = connection.prepareStatement(TextosEsp.InsertUsuarios());
                                            pet2.setString(1, nombS);
                                            pet2.setString(2, contS);
                                            pet2.setString(3, corrS);
                                            pet2.setString(4, usuS);
                                            pet2.executeUpdate();
                                            Toast.makeText(getApplicationContext(), TextosEsp.RegistroE(), Toast.LENGTH_SHORT).show();
                                            pet2.close();
                                            connection.close();
                                            Intent intent = new Intent(getApplicationContext(), InicioSesionActivity.class);
                                            startActivity(intent);
                                            finish();
                                        } catch (SQLException e) {
                                            Toast.makeText(getApplicationContext(), TextosEsp.ErrorReg(), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    else Toast.makeText(getApplicationContext(), "Correo no disponible", Toast.LENGTH_SHORT).show();
                                }
                                else Toast.makeText(getApplicationContext(), "Usuario no disponible", Toast.LENGTH_SHORT).show();
                            }
                            else Toast.makeText(getApplicationContext(), "Escriba una contraseña", Toast.LENGTH_SHORT).show();
                        }
                        else Toast.makeText(getApplicationContext(), "Escriba un correo adecuado", Toast.LENGTH_SHORT).show();
                    }
                    else Toast.makeText(getApplicationContext(), "Escriba su correo", Toast.LENGTH_SHORT).show();
                }
                else Toast.makeText(getApplicationContext(), "Escriba su nombre", Toast.LENGTH_SHORT).show();
            }
            else Toast.makeText(getApplicationContext(), "Escriba su nombre de Usuario", Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),TextosEsp.ErrorDatos(),Toast.LENGTH_SHORT).show();
        }
    }

}
