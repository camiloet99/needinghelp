Needing Help
==========

Proyecto aplicado en TIC 1, Camilo Echeverri Tamayo.

Aplicación que funciona para la creación de diferentes tipos de peticiones que, mediante unas categorías específicas, son mapeadas con la ayuda del gps para que todos los usuarios con la aplicación tengan la posibilidad de visualizar la petición y actuar en frente a ella.

Cuenta con la creación de perfiles, calificación de usuarios, popularidad e información sobre los eventos quue ocurren constantemente en la ciudad.

Con las categorías:
--------------------

+ Robo
+ Tragedia
+ Trabajo
+ Hogar
+ Recursos
+ Otros